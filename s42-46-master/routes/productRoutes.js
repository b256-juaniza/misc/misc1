const express = require("express");
const router = express.Router();
const prodController = require("../controllers/productController.js");
const auth = require("../auth.js");

module.exports = router;
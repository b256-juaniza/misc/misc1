const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
	})

module.exports = router;

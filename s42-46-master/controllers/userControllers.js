const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkIfEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0) {

			return true;

		} else {

			return false;

		}
	})
};


module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
} 

module.exports.authenticateUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if(result == null) {

			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {

				return false;
			};
		};
	});
};
